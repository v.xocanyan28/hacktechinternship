import { useDispatch } from "react-redux";
import "./userItem.css";
import { deleteUser } from "./userSlice";

export default function UserItem({ user }) {

  const dispatch = useDispatch();
  return (
    <div className="flex-contenier">
      <div className="user-item">
        <h4>
          Name: {user.name} Email: {user.email}
        </h4>
      </div>
      <div className="button">
        <button type="submit" onClick={()=>dispatch(deleteUser(user.id))}>Delete</button>
      </div>
    </div>
  );
}
