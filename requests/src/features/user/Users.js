import { useDispatch, useSelector } from "react-redux";
import { getUsers } from "./userSlice";
import UserItem from "./UserItem";
import "./userItem.css";

export default function User() {
  const dispatch = useDispatch();
  const users = useSelector((state) => state.user.users);
  return (
    <div>
      <button
        className="async-button"
        type="submit"
        onClick={() => dispatch(getUsers())}
      >
        Get Users
      </button>
      {users.map((user) => (
        <UserItem key={user.id} user={user} />
      ))}
    </div>
  );
}
