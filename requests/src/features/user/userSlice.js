import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axios from "axios";

const initialState = {
  users: [],
};

export const getUsers = createAsyncThunk("user/getUsers", async () => {
  const response = await axios.get("https://gorest.co.in/public/v2/users");
  return response.data;
});

export const userSlice = createSlice({
  name: "user",
  initialState,
  reducers: {
    deleteUser: (state, action) => {
      state.users = state.users.filter((user) => user.id !== action.payload);
    },
  },
  extraReducers: {
    [getUsers.pending]: (state) => {
      console.log("loading");
    },
    [getUsers.fulfilled]: (state, action) => {
      console.log("is load");
      state.users = action.payload;
      console.log(state.users);
    },
  },
});

export const { deleteUser } = userSlice.actions;

export default userSlice.reducer;
