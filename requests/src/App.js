import React from 'react';
import './App.css';
import User from './features/user/Users';

function App() {
  
  return (
    <div className="App">
      <User/>
    </div>
  );
}

export default App;
